# Partie 1
## Sous-partie 1 : texte
une phrase sans rien

*une phrase en italique*

**une phrase en gras**

un lien vers [fun-mooc.fr](fun-mooc.fr)

une ligne de ```python code```

## Sous-partie 2 : listes
liste à puce
* item
    * Sous-item
    * Sous-item
* item
* item

liste numérotée
1. item
2. item
3. item
## Sous-partie 3 : code
|``` # extrait de code |
